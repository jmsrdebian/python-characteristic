Source: python-characteristic
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-pytest,
Build-Depends-Indep: python3-sphinx
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://pypi.python.org/pypi/characteristic/
Vcs-Browser: https://salsa.debian.org/debian/python-characteristic
Vcs-Git: https://salsa.debian.org/debian/python-characteristic.git

Package: python3-characteristic
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: helper for implementing attribute-related object protocols (Python 3)
 characteristic is Python package with class decorators that ease the chores
 of implementing the most common attribute-related object protocols.
 .
 You just specify the attributes to work with and characteristic gives you:
 .
   - a nice human-readable __repr__,
   - a complete set of comparison methods,
   - and a kwargs-based initializer (that cooperates with your existing one)
 .
 without writing dull boilerplate code again and again.
